package com.sg.user.account.model;

import com.sg.user.account.util.AccountType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    private User user;
    private Account account;

    @BeforeEach
    @DisplayName("Create user account")
    public void setup(){
        user = new User( "rahal",  "hamdi",  "rahal.hamdi91@gmail.com", "Tunisia");
        account = new Account(AccountType.INDIVIDAL,user);
    }

    @Test
    @DisplayName("In order to save money")
    void despositMoneyToMywallet() {
        Double amount = 100.0;
        account.despositMoneyToMywallet(amount);
        Assertions.assertEquals(account.getBalanceAccount().getCreditedAmount(), 100.0);
        Assertions.assertEquals(account.getBalanceAccount().getDebitedAmount(), 0.0);
        Assertions.assertEquals(account.getWallet().getAmount(), 100.0);
    }

    @Test
    @DisplayName("In order to retrieve some or all of my savings")
    void recoverMoneyToMywallet() {
        Double amount = 100.0;
        account.despositMoneyToMywallet(amount);
        account.recoverMoneyToMywallet(50.0);

        Assertions.assertEquals(account.getBalanceAccount().getCreditedAmount(), 100.0);
        Assertions.assertEquals(account.getBalanceAccount().getDebitedAmount(), 50.0);
        Assertions.assertEquals(account.getWallet().getAmount(), 50.0);
    }

    @Test
    @DisplayName("In order to check my operations")
    void saveHistoryTransaction() {
        Double amount = 100.0;
        account.despositMoneyToMywallet(amount);
        account.recoverMoneyToMywallet(50.0);
        Assertions.assertEquals(account.getWallet().getTransactions().size(), 2);
    }
}
