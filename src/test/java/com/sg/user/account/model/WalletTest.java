package com.sg.user.account.model;

import com.sg.user.account.util.AccountType;
import com.sg.user.account.util.TransactionStatus;
import com.sg.user.account.util.TransactionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WalletTest {

    private User user;
    private Account account;
    private  Transaction transaction;

    @BeforeEach
    @DisplayName("Create user account")
    public void setup(){
        user = new User( "rahal",  "hamdi",  "rahal.hamdi91@gmail.com", "Tunisia");
        account = new Account(AccountType.INDIVIDAL,user);
        account.despositMoneyToMywallet(80.0);
    }

    @Test
    @DisplayName("Create payIn success")
    void payIn() {
        transaction = account.getWallet().payIn(100.0);
        Assertions.assertEquals(transaction.getStatus(), TransactionStatus.SUCCEEDED);
        Assertions.assertEquals(account.getWallet().getAmount(), 180.0);
    }

    @Test
    @DisplayName("Create payOut success")
    void payOut() {
        transaction = account.getWallet().payOut(20.0);
        Assertions.assertEquals(transaction.getStatus(), TransactionStatus.SUCCEEDED);
        Assertions.assertEquals(account.getWallet().getAmount(), 60.0);
    }

    @Test
    @DisplayName("Create payOut failed")
    void failedPayOut() {
        transaction = account.getWallet().payOut(100.0);
        Assertions.assertEquals(transaction.getStatus(), TransactionStatus.FAILED);
        Assertions.assertEquals(account.getWallet().getAmount(), 80.0);
    }
}
