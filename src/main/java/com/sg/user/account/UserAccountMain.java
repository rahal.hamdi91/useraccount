package com.sg.user.account;

import com.sg.user.account.model.Account;
import com.sg.user.account.model.User;
import com.sg.user.account.util.AccountType;

public class UserAccountMain {

    public static void main(String[] args) throws InterruptedException {

        User user = new User( "rahal",  "hamdi",  "rahal.hamdi91@gmail.com", "Tunisia");
        Account account = new Account(AccountType.INDIVIDAL,user);
        account.despositMoneyToMywallet(100.0);
//      account.recoverMoneyToMywallet(100.0);
        account.recoverMoneyToMywallet(10.0);
        account.despositMoneyToMywallet(80.0);
        account.printHistoriqueAccount();

    }
}
