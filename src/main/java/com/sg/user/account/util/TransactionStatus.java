package com.sg.user.account.util;

public enum TransactionStatus {
    SUCCEEDED,
    FAILED,
    CREATED,
    PINDING
}
