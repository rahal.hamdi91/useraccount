package com.sg.user.account.model;

import com.sg.user.account.util.TransactionStatus;
import com.sg.user.account.util.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Wallet {

    private UUID walletID;
    private String name;
    private Double amount;
    private Date createdIn;
    Set<Transaction> transactions;

    public Wallet(String name) {
        this.walletID = UUID.randomUUID();
        this.name = name;
        this.amount = 0.0;
        this.createdIn = new Date();
        this.transactions = new HashSet<>();
    }

    public Transaction payIn(Double amount){
        Transaction transaction = new Transaction(amount, TransactionType.PAYIN);
        this.amount += amount;
        transaction.setStatus(TransactionStatus.SUCCEEDED);
        return transaction;
    }

    public Transaction payOut(Double amount){
        Transaction transaction = new Transaction(amount,TransactionType.PAYOUT);
        if(this.amount >= amount){
            this.amount -= amount;
            transaction.setStatus(TransactionStatus.SUCCEEDED);
        }
        else{
            System.out.println("Payment Failed! Unsufficient wallet balance.");
            transaction.setStatus(TransactionStatus.FAILED);
        }
        return transaction;

    }

}
