package com.sg.user.account.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Data
@AllArgsConstructor
@ToString
public class BalanceAccount {

    private UUID accountID;
    private Double creditedAmount;
    private Double debitedAmount;

    public BalanceAccount() {
        this.accountID = UUID.randomUUID();
        this.creditedAmount = 0.0;
        this.debitedAmount = 0.0;
    }

    public void addDebitAmount(Double amount){
        this.debitedAmount += amount;
    }

    public void addCreditAmount(Double amount){
        this.creditedAmount += amount;
    }

}
