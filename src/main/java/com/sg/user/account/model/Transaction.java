package com.sg.user.account.model;

import com.sg.user.account.util.TransactionStatus;
import com.sg.user.account.util.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Transaction implements Comparable<Transaction> {

    private UUID transactionID;
    private TransactionStatus status;
    private TransactionType transactionType;
    private Double amount;
    private Date createdIn;
    private String result;

    public Transaction(Double amount,TransactionType transactionType) {
        this.transactionID = UUID.randomUUID();
        this.status = TransactionStatus.CREATED;
        this.amount = amount;
        this.createdIn = new Date();
        this.transactionType = transactionType;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
        if(status.equals(TransactionStatus.SUCCEEDED))
            this.setResult("Success");
        if(status.equals(TransactionStatus.FAILED) && this.transactionType.equals(TransactionType.PAYIN))
            this.setResult("PayIn transaction error");
        else if(status.equals(TransactionStatus.FAILED) && !this.transactionType.equals(TransactionType.PAYIN))
            this.setResult("Unsufficient wallet balance");
    }

    @Override
    public int compareTo(Transaction o) {
        if (getCreatedIn() == null || o.getCreatedIn() == null)
            return 0;
        return getCreatedIn().compareTo(o.getCreatedIn());
    }
}
