package com.sg.user.account.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private UUID userID;
    private String firstName;
    private String lastName;
    private String email;
    private String adresse;

    public User(String firstName, String lastName, String email, String adresse) {
        this.userID = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.adresse = adresse;
    }
}
