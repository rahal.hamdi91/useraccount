package com.sg.user.account.model;

import com.sg.user.account.util.AccountType;
import com.sg.user.account.util.TransactionStatus;
import com.sg.user.account.util.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor @AllArgsConstructor @ToString
public class Account {

    private UUID accountID;
    private AccountType accountType;
    private User user;
    private Date createdIn;
    private Wallet wallet;
    private BalanceAccount balanceAccount;

    public Account(AccountType accountType, User user) {
        this.accountID = UUID.randomUUID();
        this.accountType = accountType;
        this.user = user;
        this.createdIn = new Date();
        this.wallet = new Wallet("wallet account N° "+this.getAccountID());
        this.balanceAccount = new BalanceAccount();
    }

    public void despositMoneyToMywallet(Double amount){
        Transaction transaction = this.wallet.payIn(amount);
        saveHistoryTransaction(transaction);
    }

    public void recoverMoneyToMywallet(Double amount){
        Transaction transaction = this.wallet.payOut(amount);
        saveHistoryTransaction(transaction);
    }

    public void saveHistoryTransaction(Transaction transaction){
        this.wallet.getTransactions().add(transaction);
        if(transaction.getStatus().equals(TransactionStatus.SUCCEEDED)){
            if(transaction.getTransactionType().equals(TransactionType.PAYIN)){
                this.balanceAccount.addCreditAmount(transaction.getAmount());
            }else {
                this.balanceAccount.addDebitAmount(transaction.getAmount());
            }
        }
    }

    public void printHistoriqueAccount(){
        System.out.println("**************************START*********************************");
        System.out.println("Account: "+this.getUser().getLastName() + " "+this.getUser().getFirstName());
        System.out.println("Type of account: "+this.getAccountType().name());
        System.out.println("Amount of wallet: $"+this.getWallet().getAmount());
        System.out.println("Here are the recent transactions:");
        if(getWallet() != null && !getWallet().getTransactions().isEmpty()){
                    Collections.
                        sort(getWallet().getTransactions().stream().collect(Collectors.toList()), Collections.reverseOrder());
            for (Transaction t : getWallet().getTransactions()){
                System.out.println("===================");
                System.out.println("CREATION:" + new SimpleDateFormat("yyyy-MM-dd hh:MM:ss").format(t.getCreatedIn()));
                System.out.println("ID:" + t.getTransactionID());
                System.out.println("AMOUNTS:$" + t.getAmount());
                System.out.println("TYPE:" + t.getTransactionType().name());
                System.out.println("STATUS:" + t.getStatus().name());
                System.out.println("RESULT:" + t.getResult());
                System.out.println("===================");
            }
        }
        System.out.println("Credited money:$" + this.balanceAccount.getCreditedAmount());
        System.out.println("Debited money:$" + this.balanceAccount.getDebitedAmount());
        System.out.println("**************************END*********************************");
    }

}
